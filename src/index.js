import React from 'react'
import App from './App'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import store, { persistor } from '@/store'
import { PersistGate } from 'redux-persist/lib/integration/react'
import { ViewportProvider } from '@/utils/view'

const container = document.getElementById('root')
ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <ViewportProvider>
          <App />
        </ViewportProvider>
      </BrowserRouter>
    </PersistGate>
  </Provider>,
  container
)
