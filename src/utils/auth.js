import store, { dispatch } from '@/store'
import { init, setUserInfo } from '@/store/user'
import { setGlobalInfo } from '@/store/global'
// import Cookies from 'js-cookie'

// read the login status
let isLogin = store.getState().user.token ? true : false
const signIn = (username, auth, token, meta = {}) => {
  isLogin = true
  dispatch(
    setUserInfo({
      username,
      auth,
      token,
      meta
    })
  )
}
const signOut = () => {
  isLogin = false
  dispatch(init())
}

const getUser = () => store.getState().user

const setNewSignUp = (val) => {
  dispatch(setGlobalInfo({ newSignUp: val }))
}

export function useAuth() {
  return {
    signIn,
    signOut,
    getUser,
    isLogin,
    setNewSignUp
  }
}
