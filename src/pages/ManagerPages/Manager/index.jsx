import React from 'react'
import { Outlet, useParams } from 'react-router-dom'
import { Layout } from 'antd'
import ManagerHeader from './components/ManagerHeader'
import { $api } from '@/http/api'

const withRouter = (Component) => {
  return (props) => (
    <Component {...props} params={useParams()} />
  )
}

const { Header } = Layout

class Manager extends React.Component {
  state = {
    information: {
      name: ''
    }
  }

  componentDidMount() {
    $api.manager.getRestaurantInfo({
      id: this.props.params.id
    }).then((res) => {
      if (res.code === 200) {
        this.setState({
          information: res.data.information
        })
      }
    })
  }

  render() {
    const { name } = this.state.information
    return (
      <Layout className='!min-h-screen'>
        <Header className='!h-[96px] !leading-[96px] !bg-white !px-0'>
          <ManagerHeader name={name}></ManagerHeader>
        </Header>
        <Layout className='p-4'>
          <Outlet></Outlet>
        </Layout>
      </Layout>
    )
  }
}

export default withRouter(Manager)