import Available from '../RestaurantOrders/Available'
import Ongoing from '../RestaurantOrders/Ongoing'
import Finished from '../RestaurantOrders/Finished'
import { Tabs } from 'antd'

const { TabPane } = Tabs;

const Orders = () => {
  return (
    <>
      <Tabs type="card" centered defaultActiveKey="1">
        <TabPane tab="Available" key="1">
          <Available></Available>
        </TabPane>
        <TabPane tab="Ongoing" key="2">
          <Ongoing></Ongoing>
        </TabPane>
        <TabPane tab="Finished" key="3">
          <Finished></Finished>
        </TabPane>
      </Tabs>
    </>
  )
}

export default Orders