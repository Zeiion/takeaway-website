import React from 'react'
import { Empty, Pagination, message, Button } from 'antd'
import RestaurantOrder from './components/RestaurantOrder'
import { $api } from '@/http/api'
import { useParams } from 'react-router-dom'

const withRouter = (Component) => {
  return (props) => (
    <Component {...props} params={useParams()} />
  )
}

class Available extends React.Component {
  state = {
    pageNum: 1,
    pageSize: 12,
    total: 0,
    list: []
  }

  refresh = () => {
    $api.manager.getRestaurantAvailable({
      id: this.props.params.id,
      page_num: this.state.pageNum,
      page_size: this.state.pageSize
    }).then((res) => {
      if (res.code === 200) {
        this.setState({
          list: res.data.list,
          total: res.data.total,
        })
      }
    })
  }

  take = (id) => {
    $api.manager.acceptOrder({
      id: id
    }).then((res) => {
      if (res.code === 200) {
        message.success('Successfully Take Order')
        this.refresh()
      }
    })
  }

  pageChange = (page) => {
    this.setState({ pageNum: page })
    this.refresh()
  }

  componentDidMount() {
    this.refresh()
  }

  onRefresh = () => {
    this.refresh()
    message.success("Refresh successfully!")
  }

  render() {
    if (this.state.list.length === 0) {
      return (<Empty description="There is no avaliable order." className='pt-8' />)
    } else {
      return (
        <>
          <div className='mb-4'>
            <Button onClick={this.onRefresh}>Refresh</Button>
          </div>
          <div className='grid gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4'>
            {this.state.list.map((item, index) => {
              return (
                <RestaurantOrder key={index} {...item} buttonText="Take" buttonClick={this.take} popconfirm />
              )
            })}
          </div>
          <Pagination
            style={{ textAlign: 'center', marginTop: '2rem' }}
            defaultCurrent={1}
            total={this.state.total}
            PageSize={this.state.pageSize}
            onChange={this.pageChange}
          />
        </>
      )
    }
  }
}

export default withRouter(Available)