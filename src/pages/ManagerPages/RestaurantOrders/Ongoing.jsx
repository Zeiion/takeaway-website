import React from 'react'
import { Pagination, Empty, Button, message } from 'antd'
import RestaurantOrder from './components/RestaurantOrder'
import { $api } from '@/http/api'
import { useParams, useNavigate } from 'react-router-dom'

const withRouter = (Component) => {
  return (props) => (
    <Component {...props} params={useParams()} navigate={useNavigate()} />
  )
}

class Ongoing extends React.Component {
  state = {
    pageNum: 1,
    pageSize: 12,
    total: 0,
    list: []
  }

  onRefresh = () => {
    this.refresh()
    message.success("Refresh successfully!")
  }

  refresh = () => {
    $api.manager.getRestaurantOngoing({
      id: this.props.params.id,
      page_num: this.state.pageNum,
      page_size: this.state.pageSize
    }).then((res) => {
      if (res.code === 200) {
        this.setState({
          list: res.data.list,
          total: res.data.total,
        })
      }
    })
  }

  selectRider = (orderId) => {
    this.props.navigate(`/manager/${this.props.params.id}/riders/${orderId}`)
  }

  pageChange = (page) => {
    this.setState({ pageNum: page })
    this.refresh()
  }

  componentDidMount() {
    this.refresh()
  }

  render() {
    if (this.state.list.length === 0) {
      return (<Empty description="There is no ongoing order." className='pt-8' />)
    } else {
      return (
        <>
          <div className='mb-4'>
            <Button onClick={this.onRefresh}>Refresh</Button>
          </div>
          <div className='grid gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4'>
            {this.state.list.map((item, index) => {
              return (
                <RestaurantOrder key={index} {...item} buttonText="Rider" buttonClick={this.selectRider} button />
              )
            })}
          </div>
          <Pagination
            style={{ textAlign: 'center', marginTop: '2rem' }}
            defaultCurrent={1}
            total={this.state.total}
            PageSize={this.state.pageSize}
            onChange={this.pageChange}
          />
        </>
      )
    }
  }
}

export default withRouter(Ongoing)