import { useState } from 'react'
import { Card, Button, Modal } from 'antd'
import { useNavigate } from 'react-router-dom'
import Moment from 'react-moment'
import StatusTag from '@/components/StatusTag'

const CardButton = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const navigate = useNavigate();

  const detail = () => {
    navigate(`/order/${props.id}`);
  }

  const onOk = () => {
    handleCancel()
    props.buttonClick(props.id)
  }

  if (props.popconfirm) {
    return (
      <>
        <StatusTag status={props.status} />
        <Button type="link" onClick={detail}>Detail</Button>
        <Button type="primary" onClick={showModal}>{props.buttonText}</Button>
        <Modal title="Take the order" visible={isModalVisible} onOk={onOk} onCancel={handleCancel}>
          <div className='text-item'>🧑 {props.userInfo.nickname} 📞 {props.phone}</div>
          <div className='text-item'>⏰ <Moment format="YYYY-MM-DD HH:mm">{props.orderTime}</Moment> 💷 {props.totalPrice}</div>
          <div className='text-item'></div>
          <div className='text-item'></div>
          {props.food.map((item, index) => {
            return (
              <div key={index} className='text-item'>{item.name} * {item.number}</div>
            )
          })}
        </Modal>
      </>
    )
  } else if (props.button) {
    let button = null;
    if (props.status === 1) {
      button = <Button type="primary" onClick={e => props.buttonClick(props.id)}>{props.buttonText}</Button>
    }
    return (
      <>
        <StatusTag status={props.status} />
        <Button type="link" onClick={detail}>Detail</Button>
        {button}
      </>
    )
  } else {
    return (
      <>
        <StatusTag status={props.status} />
        <Button type="link" onClick={detail}>Detail</Button>
      </>
    )
  }
}

const RestaurantOrder = (props) => {
  return (
    <Card size="small" title={props.userInfo.nickname} extra={<CardButton {...props} />}>
      <div className='text-item'>⏰ <Moment format="YYYY-MM-DD HH:mm">{props.orderTime}</Moment> 💷 {props.totalPrice}</div>
      {props.food.map((item, index) => {
        return (
          <div key={index} className='text-item'>{item.name} * {item.number}</div>
        )
      })}
    </Card>
  )
}

export default RestaurantOrder