import { Card, Avatar, Modal } from 'antd'
import { RocketOutlined } from '@ant-design/icons';
import { useState } from 'react'

const { Meta } = Card;

const CardButton = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Modal title="Assign to the Rider" visible={isModalVisible} onOk={e => props.buttonClick(props.id)} onCancel={handleCancel}>
        <div className='text-item'>🧑 {props.nickname}</div>
        <div className='text-item'>🛵 {props.description}</div>
      </Modal>
      <div className='flex-center gap-2' onClick={showModal}>
        <RocketOutlined key="choose" />
        <span>Choose</span>
      </div>
    </>
  )
}

const RiderCard = (props) => {
  return (
    <Card actions={[<CardButton {...props} />]}>
      <Meta
        avatar={<Avatar src="https://joeschmoe.io/api/v1/random" />}
        title={props.nickname}
        description={props.description}
      />
    </Card>
  )
}

export default RiderCard