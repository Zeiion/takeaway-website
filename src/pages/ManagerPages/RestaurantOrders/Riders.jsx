import React from 'react'
import RiderCard from './components/RiderCard'
import { Button, Pagination, message } from 'antd'
import { $api } from '@/http/api'
import { useParams, useNavigate } from 'react-router-dom'

const withRouter = (Component) => {
  return (props) => (
    <Component {...props} params={useParams()} navigate={useNavigate()} />
  )
}

class Riders extends React.Component {
  state = {
    pageNum: 1,
    pageSize: 12,
    total: 0,
    list: [
      {
        id: 114514,
        nickname: 'Biden',
        description: 'President'
      }
    ]
  }

  refresh = () => {
    $api.manager.getAllRiders({
      page_num: this.state.pageNum,
      page_size: this.state.pageSize
    }).then((res) => {
      if (res.code === 200) {
        this.setState({
          list: res.data.list,
          total: res.data.total,
        })
      }
    })
  }

  assign = (riderid) => {
    $api.manager.assignRider({
      id: this.props.params.orderid,
      rider_id: riderid,
    }).then((res) => {
      if (res.code === 200) {
        message.success('Successfully Assign To')
        this.props.navigate(-1)
      }
    })
  }

  pageChange = (page) => {
    this.setState({ pageNum: page })
    this.refresh()
  }

  componentDidMount() {
    this.refresh()
  }

  render() {
    return (
      <>
        <div className='mb-4'>
          <Button onClick={e => {this.props.navigate(-1)}}>Back</Button>
        </div>
        <div className='grid gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5'>
          {this.state.list.map((item, index) => {
            return (
              <RiderCard key={index} {...item} buttonClick={this.assign}></RiderCard>
            )
          })}
        </div>
        <Pagination
          style={{ textAlign: 'center', marginTop: '2rem' }}
          defaultCurrent={1}
          total={this.state.total}
          PageSize={this.state.pageSize}
          onChange={this.pageChange}
        />
      </>
    )
  }
}

export default withRouter(Riders)