import React, { useState } from 'react'
import { Empty, Pagination, message, Tabs } from 'antd'
import OrderCard from './OrderCard'
import { $api } from '@/http/api'

class OngoingList extends React.Component {
  state = {
    pageNum: 1,
    pageSize: 12,
    total: 0,
    list: [],
  }

  refresh = () => {
    $api.user.getOngoingOrderList().then((res) => {
      if (res.code === 200) {
        this.setState({
          list: res.data.list,
          total: res.data.total,
        })
      }
    })
  }

  pageChange = (page) => {
    this.setState({ pageNum: page })
    this.refresh()
  }

  componentDidMount() {
    this.refresh()
  }

  render() {
    if (this.state.list.length === 0) {
      return (<Empty description="There is no ongoing order." className='pt-8' />)
    } else {
      return (
        <>
          <div className='grid gap-4 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4'>
            {this.state.list.map((item, index) => {
              return (
                <OrderCard key={index} {...item} refresh={this.refresh} />
              )
            })}
          </div>
          <Pagination
            style={{ textAlign: 'center', marginTop: '2rem' }}
            defaultCurrent={1}
            total={this.state.total}
            PageSize={this.state.pageSize}
            onChange={this.pageChange}
          />
        </>
      )
    }
  }
}

export default OngoingList