import { Tabs } from 'antd'
import OngoingList from './Ongoing'
import HistoryList from './History'

const { TabPane } = Tabs;

const OrderList = () => {
  return (
    <Tabs type="card" centered defaultActiveKey="1">
      <TabPane tab="Ongoing" key="1">
        <OngoingList></OngoingList>
      </TabPane>
      <TabPane tab="History" key="2">
        <HistoryList></HistoryList>
      </TabPane>
    </Tabs>
  )
}

export default OrderList