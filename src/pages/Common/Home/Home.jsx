import { Layout } from 'antd'
import { Outlet } from 'react-router-dom'
import NavBar from '@/components/NavBar/NavBar'

const { Header } = Layout

const Home = () => {
  return (
    <Layout className="!min-h-screen">
      <Header className="!h-[96px] !leading-[96px] !bg-white !px-0">
        <NavBar></NavBar>
      </Header>
      <Layout className="md:py-4 md:px-8">
        <Outlet></Outlet>
      </Layout>
    </Layout>
  )
}

export default Home
