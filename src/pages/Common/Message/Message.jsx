import { useState, useEffect } from 'react'
import { List, Avatar, Button, Tag, message, Badge } from 'antd'
import { $api } from '@/http/api'
import { useNavigate } from 'react-router-dom'
import Moment from 'react-moment'
const Message = () => {
  // const [tab, setTab] = useState(0)
  const navigate = useNavigate()
  const [messageList, setMessageList] = useState([
    // {
    //   id: 62,
    //   type: 0,
    //   msg: 'est esse',
    //   target: 47,
    //   isRead: true
    // },
    // {
    //   id: 59,
    //   type: 1,
    //   msg: 'laboris voluptate ea',
    //   target: 22,
    //   isRead: true
    // },
    // {
    //   id: 75,
    //   type: 1,
    //   msg: 'cupidatat elit ea',
    //   target: 52,
    //   isRead: true
    // }
  ])

  useEffect(() => {
    $api.user.getMessageList({}).then((res) => {
      if (res.code === 200) {
        setMessageList(res.data)
      }
    })
  }, [])

  // type 2 tag
  const getTypeTag = (type) => {
    const map = ['Order', 'AD']
    const colorClassMap = ['!bg-blue-500', '!bg-red-400']
    return (
      <Tag className={colorClassMap[type] + ' !text-white w-20'}>
        {map[type] || 'Unknown'}
      </Tag>
    )
  }

  //TODO
  const goToDetail = (item) => {
    if (item.type === 1) {
      // ad
      navigate('/restaurant/' + item?.target?.restaurant?.id)
    } else {
      navigate('/order/' + item?.target?.id)
    }
  }

  return (
    <>
      <div>
        <List
          itemLayout="horizontal"
          dataSource={messageList}
          bordered
          renderItem={(item) => (
            <List.Item
              className={'!px-2 ' + (item.isRead ? '!bg-grey-50' : '!bg-white')}
              actions={[
                <Button
                  type="primary"
                  ghost
                  onClick={() => {
                    goToDetail(item)
                  }}
                >
                  Detail
                </Button>
              ]}
            >
              <List.Item.Meta
                avatar={
                  <Avatar
                    className="!hidden md:!block"
                    src="https://joeschmoe.io/api/v1/random"
                  />
                }
                title={
                  <>
                    {item.name}
                    <span className="hidden ml-2 text-gray-400 md:inline ">
                      <Moment format="YYYY-MM-DD HH:mm">{item.time}</Moment>
                    </span>{' '}
                  </>
                }
                description={
                  <>
                    <div className="hidden md:block">{item.msg}</div>
                    <div className="md:hidden">
                      <Moment format="YYYY-MM-DD HH:mm">{item.time}</Moment>
                    </div>
                  </>
                }
              />
              <div className="pl-3 -mr-2 text-center">
                {getTypeTag(item.type)}
              </div>
            </List.Item>
          )}
        />
      </div>
    </>
  )
}

export default Message
