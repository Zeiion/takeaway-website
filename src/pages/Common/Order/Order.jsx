import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { $api } from '@/http/api'
import { Progress } from 'antd'
import Moment from 'react-moment'
const Order = (props) => {
  const { id } = useParams()
  const [distance, setDistance] = useState(2)
  const [orderInfo, setOrderInfo] = useState({
    id: 56,
    userInfo: {
      nickname: '金刚',
      description: 'Duis non consectetur ipsum ut',
      sex: 'mollit do',
      identity: 20,
      email: 'm.gdpyu@qq.com'
    },
    orderTime: '1993-12-28 13:21:09',
    estimatedTime: '1998-08-03 21:34:05',
    address: '广西壮族自治区防城港市乌兰县',
    phone: '18655973118',
    food: [],
    comment: 'nulla dolore dolore aliqua aute',
    status: 3,
    riderId: 65,
    finishTime: null
  })
  const getOrderDistance = () => {
    $api.order.getOrderDistance({ id }).then((res) => {
      if (res.code === 200) {
        setDistance(res.data.distance_km)
      }
    })
  }
  useEffect(() => {
    $api.order
      .getOrderInfo({
        id
      })
      .then((res) => {
        if (res.code === 200) {
          setOrderInfo(res.data)
        }
      })

    // get order distance
    const timer = setInterval(() => {
      getOrderDistance()
    }, 5000)

    return () => {
      clearInterval(timer)
    }
  }, [])

  const orderProgress = (status = 0) => {
    // canceled
    if (status === 5)
      return <Progress percent={100} showInfo={false} status="exception" />
    const total = 4
    const formArray = (num) => new Array(num).fill(null)
    return (
      <>
        {formArray(status).map((_, index) => (
          <Progress percent={100} showInfo={false} key={'fill-' + index} />
        ))}
        {formArray(total - status).map((_, index) => (
          <Progress percent={0} showInfo={false} key={'empty-' + index} />
        ))}
      </>
    )
  }

  const orderText = (status = 0) => {
    const txt = [
      'Please wait restaurant take the order',
      'Please wait assign delivery',
      'Please wait delivery staff pick up the order',
      'Delivering',
      'Arrived',
      'Canceled'
    ]
    return txt[status >= txt.length ? txt.length - 1 : status]
  }

  return (
    <>
      <div className="flex items-center justify-center h-16 text-xl ">
        <span className="font-bold">Estimate Time </span> : &nbsp;
        {orderInfo?.estimatedTime ? (
          <Moment format="YYYY-MM-DD HH:mm">{orderInfo?.estimatedTime}</Moment>
        ) : (
          '--:--:--'
        )}
      </div>
      <div className="flex gap-2 px-4">{orderProgress(orderInfo?.status)}</div>
      <div className="flex justify-center gap-2 px-2 pt-2">
        {orderText(orderInfo?.status)}
      </div>
      {orderInfo.status === 3 && (
        <div className="relative w-full h-48 mx-auto text-right bg-left bg-no-repeat bg-location-card md:w-1/3">
          <span className="absolute right-4 bottom-10 max-w-[58vw] text-lg">
            The delivery staff is
            <span className="text-xl font-bold">
              {' ' + (distance || '?') + 'km'}
            </span>{' '}
            away from you
          </span>
        </div>
      )}
    </>
  )
}

export default Order
