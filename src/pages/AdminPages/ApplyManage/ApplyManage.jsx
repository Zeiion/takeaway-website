import { useState, useEffect } from 'react'
import { List, Avatar, Button, Tag, message } from 'antd'
import { $api } from '@/http/api'
import Moment from 'react-moment'
import { useNavigate } from 'react-router-dom'

const ApplyManage = () => {
  const navigate = useNavigate()
  const [tab, setTab] = useState(0)
  const [newList, setNewList] = useState([
    {
      user: {
        nickname: 'mana',
        description: 'aute elit irure',
        sex: 'amet Lorem',
        identity: 1,
        email: 'l.pqitzqif@qq.com'
      },
      request_time: '1995-07-06 06:21:07',
      request_id: 62,
      target_identify: 2
    }
  ])
  const [finishList, setFinishList] = useState([
    {
      user: {
        nickname: 'mana',
        description: 'aute elit irure',
        sex: 'amet Lorem',
        identity: 1,
        email: 'l.pqitzqif@qq.com'
      },
      request_time: '1995-07-06 06:21:07',
      request_id: 62,
      target_identify: 2,
      status: 1
    }
  ])

  useEffect(() => {
    $api.admin.getApplyList().then((res) => {
      if (res.code === 200) {
        setNewList(res.data.list)
      }
    })
  }, [])

  const onTabChange = (val) => {
    if (val !== tab) setTab(val)
  }

  // auth 2 text
  const getAuthTag = (auth) => {
    const map = ['User', 'Delivery', 'Restaurant', 'Admin']
    const colorClassMap = [
      '!bg-green-500',
      '!bg-blue-500',
      '!bg-red-400',
      '!bg-purple-500'
    ]
    return (
      <Tag className={colorClassMap[auth] + ' !text-white w-20'}>
        {map[auth] || 'Unknown'}
      </Tag>
    )
  }

  // status 2 text
  const getStatusTag = (status) => {
    const map = ['Rejected', 'Accepted']
    const colorClassMap = ['!bg-red-400', '!bg-green-500']
    return (
      <Tag className={colorClassMap[status] + ' !text-white w-20'}>
        {map[status] || 'Unknown'}
      </Tag>
    )
  }
  const reject = (item) => {
    $api.admin
      .putApply({
        id: item.request_id,
        success: 0,
        type: item.type
      })
      .then((res) => {
        if (res.code === 200) {
          message.success('reject success')
          navigate(0)
        } else {
          message.error('reject failed')
        }
      })
  }
  const accept = (item) => {
    $api.admin
      .putApply({
        id: item.request_id,
        success: 1,
        type: item.type
      })
      .then((res) => {
        if (res.code === 200) {
          message.success('accept success')
          navigate(0)
        } else {
          message.error('accept failed')
        }
      })
  }

  return (
    <>
      {/* <div className="flex w-full h-[6vh] bg-slate-50">
        <button
          onClick={() => {
            onTabChange(0)
          }}
          className={[
            'w-1/2',
            'transition',
            tab === 0 ? 'bg-slate-200' : null
          ].join(' ')}
        >
          NEW
        </button>
        <button
          onClick={() => {
            onTabChange(1)
          }}
          className={[
            'w-1/2',
            'transition',
            tab === 1 ? 'bg-slate-200' : null
          ].join(' ')}
        >
          FINISH
        </button>
      </div> */}
      {tab === 0 ? (
        <div>
          <List
            itemLayout="horizontal"
            dataSource={newList}
            renderItem={(item) => (
              <List.Item
                className="!px-2 bg-white"
                actions={[
                  <Button
                    danger
                    onClick={() => {
                      reject(item)
                    }}
                  >
                    reject
                  </Button>,
                  <Button
                    type="primary"
                    ghost
                    onClick={() => {
                      accept(item)
                    }}
                  >
                    accept
                  </Button>
                ]}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      className="!hidden md:!block"
                      src="https://joeschmoe.io/api/v1/random"
                    />
                  }
                  title={
                    <>
                      {item.user.nickname}
                      <span className="hidden ml-2 text-gray-400 md:inline ">
                        <Moment format="YYYY-MM-DD HH:mm">
                          {item.request_time}
                        </Moment>
                      </span>{' '}
                    </>
                  }
                  description={
                    <>
                      <div className="hidden md:block">
                        {item.user.description}
                      </div>
                      <div className="md:hidden">
                        <Moment format="YYYY-MM-DD HH:mm">
                          {item.request_time}
                        </Moment>
                      </div>
                    </>
                  }
                />
                <div className="pl-3 -mr-2 text-center">
                  {getAuthTag(item.target_identify)}
                </div>
              </List.Item>
            )}
          />
        </div>
      ) : (
        <div>
          <List
            itemLayout="horizontal"
            dataSource={finishList}
            renderItem={(item) => (
              <List.Item
                className="!px-2 bg-white"
                actions={[getStatusTag(item.status)]}
              >
                <List.Item.Meta
                  avatar={
                    <Avatar
                      className="!hidden md:!block"
                      src="https://joeschmoe.io/api/v1/random"
                    />
                  }
                  title={
                    <>
                      {item.user.nickname}
                      <span className="hidden ml-2 text-gray-400 md:inline ">
                        {item.request_time}
                      </span>{' '}
                    </>
                  }
                  description={
                    <>
                      <div className="hidden md:block">
                        {item.user.description}
                      </div>
                      <div className="md:hidden">{item.request_time}</div>
                    </>
                  }
                />
                <div className="pl-3 -mr-2 text-center">
                  {getAuthTag(item.target_identify)}
                </div>
              </List.Item>
            )}
          />
        </div>
      )}
    </>
  )
}

export default ApplyManage
