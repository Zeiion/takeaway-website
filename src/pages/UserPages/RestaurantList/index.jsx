import React from 'react'

import RestaurantList from './components/RestaurantList'
import { Layout } from 'antd'

class Restaurants extends React.Component {
  constructor() {
    super()
    this.state = { restaurantList: [1, 2, 3] }
  }
  updateRestaurantList(value) {
    this.setState({ restaurantList: value })
  }
  render() {
    const { Header, Content } = Layout
    return (
      <div className="resturants">
        {/* <Layout> */}
        {/* <Header>
            <SearchBox
              updateRestaurantList={this.updateRestaurantList.bind(this)}
            ></SearchBox>
          </Header> */}
        {/* <Content> */}
        <RestaurantList></RestaurantList>
        {/* </Content>
        </Layout> */}
      </div>
    )
  }
}
export default Restaurants
