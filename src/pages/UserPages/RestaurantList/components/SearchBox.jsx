import React from 'react'
import { Input } from 'antd'
import { $api } from '@/http/api'
class SearchBox extends React.Component {
  constructor() {
    super()
    this.state = { value: '' }
  }
  onInputChange(e) {
    this.setState({ value: e.target.value })
  }
  onSearch() {
    const data = { value: this.state.value }
    const { updateRestaurantList } = this.props
    updateRestaurantList([1, 2, 3, 4, 5, 6])
    // $api.restaurant.searchRestaurant(data).then((res) => {
    //   console.log(res)
    // })
  }
  render() {
    const { Search } = Input
    return (
      <div className="SearchBox" style={{ width: '50vw', margin: '0 auto' }}>
        <Search
          placeholder="search your restaurants"
          value={this.state.value}
          onChange={this.onInputChange.bind(this)}
          enterButton="Search"
          size="large"
          onSearch={this.onSearch.bind(this)}
        />
      </div>
    )
  }
}
export default SearchBox
