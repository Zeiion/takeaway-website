import React from 'react'
import { Card, Col, Row } from 'antd'
import { Pagination, Empty } from 'antd'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { $api } from '@/http/api'
const withRouter = (Component) => {
  return (props) => (
    <Component
      {...props}
      navigate={useNavigate()}
      location={useLocation()}
      params={useParams()}
    />
  )
}
class RestaurantList extends React.Component {
  constructor(props) {
    super(props)
    this.state = { page: 1, restaurantList: [] }

    if (!props.location.state || !props.location.state.value) {
      $api.restaurant
        .getNearRestaurant({ latitude: 62.3, longitude: 36.2 })
        .then((res) => {
          if (res.code === 200) {
            this.setState({
              restaurantList: res.data.list.map((item) => item.restaurant)
            })
          } else {
          }
        })
    } else {
      console.log({ value: props.location.state.value })
      $api.restaurant
        .searchRestaurant({ value: props.location.state.value })
        .then((res) => {
          if (res.code === 200) {
            this.setState({ restaurantList: res.data })
          } else {
          }
        })
    }
  }
  componentDidUpdate(preprops) {
    if (!this.props.location['state']) {
      return
    }
    if (
      !preprops.location.state ||
      preprops.location.state.value !== this.props.location.state.value
    ) {
      console.log({ value: this.props.location.state.value })
      $api.restaurant
        .searchRestaurant({ value: this.props.location.state.value })
        .then((res) => {
          if (res.code === 200) {
            this.setState({ restaurantList: res.data })
          }
        })
    }
  }
  onPageChange(page) {
    this.setState({ page: page })
  }
  onClickRestaurant(item) {
    this.props.navigate('/restaurant/' + item.id)
  }
  render() {
    const { Meta } = Card
    const pageSize = 12
    const start = (this.state.page - 1) * pageSize
    const end = start + pageSize

    return (
      <div className="RestaurantList">
        {!!this.props.location.state.value || (
          <div className="mb-3 text-3xl">nearby restaurants</div>
        )}
        {!this.state.restaurantList || !this.state.restaurantList.length ? (
          <Empty description={<span>no restaurant in result</span>} />
        ) : (
          <>
            <Row gutter={[16, 24]}>
              {this.state.restaurantList
                .slice(start, end)
                .map((item, index) => {
                  return (
                    <Col xs={24} xl={6} md={12} key={index}>
                      <Card
                        onClick={() => this.onClickRestaurant.bind(this)(item)}
                        hoverable
                        cover={
                          <img
                            alt="example"
                            src={
                              item.img
                                ? item.img
                                : 'http://p0.meituan.net/dealwatera/188ecde949cf5abea462be14f206d383117991.jpg'
                            }
                            className="object-cover w-40 h-60"
                          />
                        }
                      >
                        <Meta
                          title={item.name}
                          description={item.description}
                        />
                      </Card>
                    </Col>
                  )
                })}
            </Row>
            <div className="mt-5">
              <Pagination
                style={{ textAlign: 'center' }}
                defaultCurrent={1}
                total={this.state.restaurantList.length}
                PageSize={12}
                onChange={this.onPageChange.bind(this)}
              />
            </div>
          </>
        )}
      </div>
    )
  }
}

export default withRouter(RestaurantList)
