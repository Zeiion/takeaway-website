import React from 'react'
import HeadBox from './components/HeadBox'
import FoodList from './components/FoodList'
import { useParams, useNavigate } from 'react-router-dom'
import { $api } from '@/http/api'
const withRouter = (Component) => {
  return (props) => (
    <Component {...props} navigate={useNavigate()} params={useParams()} />
  )
}

class Restaurant extends React.Component {
  constructor() {
    super()
    this.state = {
      store: [],
      restaurant: {},
      foodList: []
    }
  }
  componentDidMount() {
    console.log(this.props)
    console.log({ id: parseInt(this.props.params.id) })
    $api.restaurant
      .getRestaurant({ id: parseInt(this.props.params.id) })
      .then((res) => {
        if (res.code === 200) {
          console.log(res.data)
          this.setState({
            store: res.data.foodList,
            foodList: res.data.foodList,
            restaurant: res.data.information
          })
        }
      })
  }
  searchFood(word) {
    const tempList = []
    this.state.store.forEach((item) => {
      if (item.name.indexOf(word) >= 0) {
        tempList.push(item)
      }
    })
    console.log(tempList)
    this.setState({ foodList: tempList })
  }
  render() {
    return (
      <div className="resturant">
        <HeadBox
          information={this.state.restaurant}
          searchFood={this.searchFood.bind(this)}
        ></HeadBox>

        <FoodList foodList={this.state.foodList}></FoodList>
      </div>
    )
  }
}
export default withRouter(Restaurant)
