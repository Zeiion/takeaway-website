import React from 'react'
import { Card, Col, Row } from 'antd'
import { Pagination } from 'antd'
import { Modal } from 'antd'
import { Button, InputNumber, Space, message, Empty } from 'antd'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { $api } from '@/http/api'
import { Content } from 'antd/lib/layout/layout'
const withRouter = (Component) => {
  return (props) => (
    <Component
      {...props}
      navigate={useNavigate()}
      location={useLocation()}
      params={useParams()}
    />
  )
}
class FoodList extends React.Component {
  constructor(props) {
    super()
    console.log(props.foodList)
    this.state = {
      page: 1,
      food: { name: '123', price: 5, img: '123', count: 1 },
      showModal: false,
      foodCount: 1
    }
  }

  onPageChange(page) {
    this.setState({ page: page })
  }
  onClickfood(item) {
    this.setState({ food: item, showModal: true })
  }

  handleOk() {
    this.setState({ showModal: false })
  }

  handleCancel() {
    this.setState({ foodCount: 1 })
    this.setState({ showModal: false })
  }
  onFoodCountChange(value) {
    this.setState({ foodCount: value })
  }
  addToCart() {
    $api.cart
      .addToCart({ count: this.state.foodCount, id: this.state.food.id })
      .then((res) => {
        if (res.code === 200) {
          message.success('add successfully!')
          this.setState({ showModal: false })
        }
      })
  }
  render() {
    const { Meta } = Card
    const pageSize = 12
    const start = (this.state.page - 1) * pageSize
    const end = start + pageSize
    return (
      <div className="mt-4 foodList">
        {!this.props.foodList || !this.props.foodList.length ? (
          <Empty description={<span>no food in restaurant</span>} />
        ) : (
          <>
            <Row gutter={16}>
              {this.props.foodList.slice(start, end).map((item, index) => {
                return (
                  <Col xs={24} xl={6} md={12} key={index}>
                    <Card
                      onClick={() => this.onClickfood.bind(this)(item)}
                      hoverable
                      cover={
                        <img
                          className="object-cover w-40 h-60"
                          alt="example"
                          src={
                            item.img
                              ? item.img
                              : 'http://p0.meituan.net/dealwatera/188ecde949cf5abea462be14f206d383117991.jpg'
                          }
                        />
                      }
                    >
                      <Meta
                        title={
                          <div
                            className="px-3"
                            style={{
                              display: 'flex',
                              justifyContent: 'space-between'
                            }}
                          >
                            <span className="text-lg">{item.name}</span>
                            <span style={{ color: 'rgb(252, 85, 49)' }}>
                              ${(item.price * item.discount).toFixed(2)}
                              {item.discount == 1 || (
                                <>
                                  &nbsp;&nbsp;
                                  <span
                                    style={{
                                      color: 'rgb(160,160,160)',
                                      textDecoration: 'line-through',

                                      fontWeight: 300
                                    }}
                                  >
                                    ${item.price.toFixed(2)}
                                  </span>
                                </>
                              )}
                            </span>
                          </div>
                        }
                        description={
                          <div style={{ textAlign: 'center' }}>
                            {item.description}
                          </div>
                        }
                      />
                    </Card>
                  </Col>
                )
              })}
            </Row>
            <Modal
              visible={this.state.showModal}
              onOk={this.handleOk.bind(this)}
              onCancel={this.handleCancel.bind(this)}
              width={400}
              footer={[
                <Space>
                  <InputNumber
                    min={1}
                    max={99}
                    style={{ width: 200 }}
                    value={this.state.foodCount}
                    onChange={this.onFoodCountChange.bind(this)}
                    addonAfter={
                      'in total $' +
                      (
                        this.state.foodCount *
                        this.state.food.price *
                        this.state.food.discount
                      ).toFixed(2)
                    }
                  />
                  <Button type="primary" onClick={this.addToCart.bind(this)}>
                    Add to order
                  </Button>
                </Space>
              ]}
            >
              <div className="mt-4">
                <img
                  src={
                    this.state.food.img
                      ? this.state.food.img
                      : 'http://p0.meituan.net/dealwatera/188ecde949cf5abea462be14f206d383117991.jpg'
                  }
                />
              </div>

              <div className="mt-5">
                <div
                  className="px-4"
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <span className="text-lg">{this.state.food.name}</span>
                  <span style={{ color: 'rgb(252, 85, 49)' }}>
                    ${this.state.food.price * this.state.food.discount}
                    {this.state.food.discount == 1 || (
                      <>
                        &nbsp;&nbsp;
                        <span
                          style={{
                            color: 'rgb(160,160,160)',
                            textDecoration: 'line-through',

                            fontWeight: 300
                          }}
                        >
                          ${this.state.food.price}
                        </span>
                      </>
                    )}
                  </span>
                </div>
                <div style={{ textAlign: 'center' }}>
                  {this.state.food.description}
                </div>
              </div>
            </Modal>
            <div className="mt-4">
              <Pagination
                style={{ textAlign: 'center' }}
                defaultCurrent={1}
                total={this.props.foodList.length}
                PageSize={12}
                onChange={this.onPageChange.bind(this)}
              />
            </div>
          </>
        )}
      </div>
    )
  }
}

export default withRouter(FoodList)
