import React from 'react'
import { Space } from 'antd'
import { Input } from 'antd'
import { $api } from '@/http/api'
class HeadBox extends React.Component {
  constructor() {
    super()
    this.state = { value: '' }
  }
  onInputChange(e) {
    this.setState({ value: e.target.value })
  }
  render() {
    const { Search } = Input
    const { information, searchFood } = this.props

    return (
      <div
        className="search-box"
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-around'
        }}
      >
        <div className="information">
          <div className="name" style={{ fontSize: '36px', fontWeight: 600 }}>
            {information.name}
          </div>
          <div className="address">{information.address}</div>
        </div>
        <Search
          style={{ width: '50%' }}
          placeholder="search food in the restaurant"
          value={this.state.value}
          onChange={this.onInputChange.bind(this)}
          enterButton="Search"
          size="large"
          onSearch={searchFood}
        />
      </div>
    )
  }
}
export default HeadBox
