import { LogoutOutlined } from '@ant-design/icons'
import { useAuth } from '@/utils/auth'
import { useParams, useNavigate } from 'react-router-dom'

const ManagerHeader = (props) => {
  const navigate = useNavigate()

  const { signOut, setNewSignUp } = useAuth()

  const logout = () => {
    signOut()
    navigate('/login')
  }

  const home = () => {
    navigate('/')
  }

  return (
    <div className='flex-between h-full px-4 sm:px-8 lg:px-12'>
      <div className='w-48 text-3xl leading-none cursor-pointer' onClick={home}>TAKAWAY</div>
      <div className='flex-end'>
        <div className='bg-slate-500 text-white rounded-full h-10 leading-10 px-5 mx-5'>{props.name}</div>
        <LogoutOutlined className='bg-slate-500 rounded-full' style={{
          color: '#FFF',
          fontSize: '150%',
          padding: '9.5px'
        }} onClick={logout} />
      </div>
    </div>
  )
}

export default ManagerHeader