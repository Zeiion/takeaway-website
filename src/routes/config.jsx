import { lazy } from 'react'
import { lazyLoad } from './index'
import { Outlet } from 'react-router-dom'

// define routes
const routes = [
  {
    path: '/',
    element: <Outlet />,
    children: [
      {
        path: '/login',
        element: lazyLoad(lazy(() => import('@/pages/Common/Login/Login')))
      },
      {
        path: '/register',
        element: lazyLoad(
          lazy(() => import('@/pages/Common/Register/Register'))
        )
      },
      {
        path: '/manager/:id',
        meta: {
          auth: 2
        },
        element: lazyLoad(lazy(() => import('@/pages/ManagerPages/Manager'))),
        children: [
          {
            path: 'orders',
            element: lazyLoad(
              lazy(() => import('@/pages/ManagerPages/Manager/Orders'))
            )
          },
          {
            path: 'riders/:orderid',
            element: lazyLoad(
              lazy(() => import('@/pages/ManagerPages/RestaurantOrders/Riders'))
            )
          },
          {
            path: 'menu',
            element: lazyLoad(lazy(() => import('@/pages/ManagerPages/Menu')))
          }
        ]
      },
      {
        path: '/rider',
        meta: {
          auth: 1
        },
        element: lazyLoad(lazy(() => import('@/pages/RiderPages/Rider')))
      },
      {
        path: '/',
        meta: {
          auth: 0
        },
        redirect: 'restaurant/list',
        element: lazyLoad(lazy(() => import('@/pages/Common/Home/Home'))),
        children: [
          {
            path: 'restaurant/list',
            element: lazyLoad(
              lazy(() => import('@/pages/UserPages/RestaurantList/index'))
            )
          },
          {
            path: 'restaurant/:id',
            element: lazyLoad(
              lazy(() => import('@/pages/UserPages/Restaurant/index'))
            )
          },
          {
            path: 'admin/apply',
            meta: {
              auth: 3
            },
            element: lazyLoad(
              lazy(() => import('@/pages/AdminPages/ApplyManage/ApplyManage'))
            )
          },
          {
            path: 'message',
            element: lazyLoad(
              lazy(() => import('@/pages/Common/Message/Message'))
            )
          },
          {
            path: 'order/:id',
            element: lazyLoad(lazy(() => import('@/pages/Common/Order/Order')))
          },
          {
            path: 'orders',
            element: lazyLoad(lazy(() => import('@/pages/Common/OrderList')))
          }
        ]
      }
    ]
  },
  // 404 not found
  {
    path: '*',
    meta: {
      auth: null
    },
    element: lazyLoad(lazy(() => import('@/pages/Common/404/404')))
  }
]

export default routes
