import user from './apis/user'
import restaurant from './apis/restaurant'
import cart from './apis/cart'
import admin from './apis/admin'
import manager from './apis/manager'
import rider from './apis/rider'
import order from './apis/order'
export const $api = {
  user,
  restaurant,
  cart,
  admin,
  manager,
  rider,
  order
}
