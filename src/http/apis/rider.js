import http from '../http'

const rider = {
  getRiderInfo() {
    return http('get', '/info', {})
  },

  getMyOrders() {
    return http('get', '/rider/myOrder', {})
  },

  deliver(data) {
    return http('put', '/rider/deliver', data)
  },

  arrive(data) {
    return http('put', '/rider/arrive', data)
  },
}

export default rider