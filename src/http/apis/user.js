import http from '../http'

const user = {
  /**
   * login request
   */
  login(data) {
    return http('post', '/login', data, { type: 'json' })
  },

  /**
   * logout request
   */
  logout(data) {
    return http('post', '/logout', data)
  },

  /**
   * register request
   */
  register(data) {
    return http('post', '/register', data, { type: 'json' })
  },

  changeAuth(data) {
    return http('put', '/changeIdentity', data)
  },
  applyRes(data) {
    return http('post', '/restaurant/register', data, { type: 'json' })
  },
  getMessageList() {
    return http('get', '/message/list')
  },
  getOngoingOrderList() {
    return http('get', '/order/unfinished')
  },
  getHistoryOrderList() {
    return http('get', '/order/historyOrderInfo')
  },
  cancelOrder(data) {
    return http('put', '/order/cancel', data)
  }
}

export default user
