import http from '../http'

const cart = {
  /**
   * login request
   */
  addToCart(data) {
    return http('post', '/cart/add', data)
  },
  getCart(data) {
    return http('post', '/cart/get', data)
  },
  deleteFood(data) {
    return http('post', '/cart/delete', data)
  },
  changeFoodCount(data) {
    return http('post', '/cart/count/change', data)
  },
  submitOrder(data) {
    return http('post', '/order/new', data, { type: 'json' })
  }
}

export default cart
