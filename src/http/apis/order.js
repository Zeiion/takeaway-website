import http from '../http'

const order = {
  getOrderInfo(params) {
    return http('get', '/order/orderInfo', params)
  },
  getOrderDistance(params) {
    return http('get', '/distance', params)
  }
}

export default order
