import http from '../http'

const manager = {
  getRestaurantInfo(data) {
    return http('post', '/restaurant/get', data)
  },

  getRestaurantAvailable(data) {
    return http('get', '/order/restaurantUnreceivedOrderInfo', data)
  },

  getRestaurantOngoing(data) {
    return http('get', '/order/restaurantUnfinishedOrderInfo', data)
  },

  getRestaurantFinished(data) {
    return http('get', '/order/restaurantFinishedOrderInfo', data)
  },

  acceptOrder(data) {
    return http('put', '/restaurant/acceptOrder', data)
  },

  getAllRiders(data) {
    return http('get', '/restaurant/allRider', data)
  },

  assignRider(data) {
    return http('put', '/restaurant/assignRider', data)
  },

  deleteFood(data) {
    return http('delete', '/restaurant/food', data)
  },

  addFood(data) {
    return http('post', '/restaurant/food', data)
  },

  newDiscount(data) {
    return http('put', '/restaurant/discount', data)
  },

  restaurantPicture(data) {
    return http('put', '/restaurant/picture', data)
  }
}

export default manager