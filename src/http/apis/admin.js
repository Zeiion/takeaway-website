import http from '../http'

const admin = {
  getApplyList() {
    return http('get', '/admin/identityNotice')
  },
  putApply(params) {
    return http('put', '/admin/identityNotice', params)
  }
}

export default admin
