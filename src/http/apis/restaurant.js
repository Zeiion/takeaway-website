import http from '../http'

const restaurant = {
  /**
   * login request
   */
  searchRestaurant(data) {
    return http('post', '/restaurant/search', data)
  },
  getRestaurant(data) {
    return http('post', '/restaurant/get', data)
  },
  getNearRestaurant(data) {
    return http('post', '/restaurant/near', data)
  }
}

export default restaurant
