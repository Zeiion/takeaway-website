import { createSlice } from '@reduxjs/toolkit'

const initState = () => {
  return {
    newSignUp: false
  }
}
const globalSlice = createSlice({
  name: 'global',
  initialState: initState,
  reducers: {
    init(state) {
      const initS = initState()
      Object.keys(initS).forEach((key) => {
        state[key] = initS[key]
      })
    },
    setGlobalInfo(state, action) {
      const inputState = action.payload
      Object.keys(inputState).forEach((key) => {
        state[key] = inputState[key]
      })
    }
  }
})

export const { init, setGlobalInfo } = globalSlice.actions

export default globalSlice.reducer
