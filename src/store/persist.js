import storage from 'redux-persist/lib/storage' // use localStorage

export const persistConfig = {
  key: 'root',
  version: 1,
  storage,
  whitelist: ['user'] // enable whitelist to cache data in localStorage
}
