import { combineReducers } from '@reduxjs/toolkit'
import userReducer from './user'
import globalReducer from './global'

const rootReducer = combineReducers({
  user: userReducer,
  global: globalReducer
})

export default rootReducer
