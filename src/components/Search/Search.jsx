import { useNavigate } from 'react-router-dom'
import { SearchOutlined } from '@ant-design/icons'
import { useState } from 'react'

import { message } from 'antd'
const Search = () => {
  const navigate = useNavigate()
  const [value, setValue] = useState('')
  const onInputChange = (e) => setValue(e.target.value)
  const search = (e) => {
    if (e.keyCode === 13) {
      if (value !== '') {
        navigate('/restaurant/list', {
          state: { value: value }
        })
      } else {
        message.info('please input word')
      }
    }
  }
  return (
    <div className="flex items-center w-3/4 h-12 px-3 leading-none bg-gray-200 rounded-full md:w-1/2 ">
      <SearchOutlined className="w-5 mr-2" />
      <input
        type="text"
        value={value}
        onChange={onInputChange}
        className="w-full h-8 p-0 m-0 bg-transparent border-0 outline-none text-ellipsis"
        onKeyDown={search}
        placeholder="Search Restaurants"
      />
    </div>
  )
}

export default Search
