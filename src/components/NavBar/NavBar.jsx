import { Menu } from 'antd'
import Search from '@/components/Search/Search'
import Avatar from '@/components/Avatar/Avatar'
import Cart from '@/components/Cart/Cart'
import SideMenu from '@/components/Mobile/SideMenu/SideMenu'
import { useNavigate } from 'react-router-dom'
const navs = [
  {
    key: '/',
    label: 'Home'
  }
]

const NavBar = () => {
  const navigate = useNavigate()
  // navigate
  const onNavSelect = (item) => {
    navigate(item.key)
  }
  return (
    <div className="flex items-center justify-between h-full px-4 sm:px-8 lg:px-12">
      <div className="md:hidden">
        <SideMenu></SideMenu>
      </div>
      <div
        className="hidden w-24 text-3xl leading-none cursor-pointer md:block"
        onClick={() => {
          navigate('/')
        }}
      >
        TAKAWAY
      </div>
      <div className="hidden w-2 md:block"></div>
      {/* <div className="hidden w-48 md:block shrink-0">
        <Menu mode="horizontal" items={navs} onSelect={onNavSelect} />
      </div> */}
      <Search></Search>
      <div className="hidden w-10 md:block"></div>
      <div className="flex items-center gap-8">
        <Cart></Cart>
        <Avatar></Avatar>
      </div>
    </div>
  )
}

export default NavBar
