import { Tag } from 'antd'

const StatusTag = (props) => {
    let tag = null;
    switch (props.status) {
        case 0:
            tag = <Tag color="green">Available</Tag>
            break
        case 1:
            tag = <Tag color="cyan">No Rider</Tag>
            break
        case 2:
            tag = <Tag color="blue">Waiting</Tag>
            break
        case 3:
            tag = <Tag color="geekblue">Delivering</Tag>
            break
        case 4:
            tag = <Tag color="geekblue">Finished</Tag>
            break
        case 5:
            tag = <Tag>Canceled</Tag>
            break
    }
    return tag;
}

export default StatusTag