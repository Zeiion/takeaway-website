import { Avatar, Badge } from 'antd'
import {
  CarFilled,
  ShopFilled,
  SafetyCertificateFilled
} from '@ant-design/icons'
import { useAuth } from '@/utils/auth'
const AvatarIcon = () => {
  const { getUser } = useAuth()
  const authIcon = (auth) => {
    const icons = [
      null,
      <CarFilled style={{ color: '#0499fd' }} />,
      <ShopFilled style={{ color: '#ff8e27' }} />,
      <SafetyCertificateFilled style={{ color: '#659b66' }} />
    ]
    return icons[auth]
  }
  return (
    <div>
      <Badge count={authIcon(getUser().auth || 0)}>
        <Avatar
          src="https://joeschmoe.io/api/v1/random"
          className="border-2 border-slate-100"
        />
      </Badge>
    </div>
  )
}

export default AvatarIcon
