import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAuth } from '@/utils/auth'
import { Dropdown, Menu, Space } from 'antd'
import store from '@/store'
import AuthCard from '@/components/AuthCard/AuthCard'
import AvatarIcon from '@/components/AvatarIcon/AvatarIcon'

const Avatar = () => {
  const navigate = useNavigate()
  const { signOut, setNewSignUp, getUser } = useAuth()
  const [visible, setVisible] = useState(false)

  const handleVisibleChange = (flag) => {
    setVisible(flag)
  }

  let restaurant_id = getUser().meta.restaurantId

  const handleMenuClick = (e) => {
    switch (e.key) {
      case '0':
        navigate('/message')
        break
      case '1':
        setNewSignUp(true)
        break
      case '2':
        navigate('/login')
        signOut()
        break
      case '3':
        navigate('/orders')
        break
      case '4':
        navigate(`/manager/${restaurant_id}/orders`)
        break
      case '5':
        navigate('/rider')
        break
      case '6':
        navigate('/admin/apply')
        break
      default:
        break
    }
    setVisible(false)
  }

  let items = [
    { label: 'Message', key: '0' },
    { label: 'Join as Manager/Delivery Staff', key: '1' },
    { label: 'My Orders', key: '3' }
  ]

  let auth = getUser().auth

  if (auth === 1) {
    items = items.concat({ label: 'Delivery', key: '5' })
  } else if (auth === 2 || restaurant_id >= 0) {
    items = items.concat({ label: 'My Restaurant', key: '4' })
  } else if (auth === 3) {
    items = items.concat({ label: 'Manage Apply', key: '6' })
  }

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[...items, { label: 'Log Out', key: '2' }]}
    />
  )

  return (
    <div>
      <Dropdown
        // trigger={['click']}
        overlay={menu}
        onVisibleChange={handleVisibleChange}
        visible={visible}
      >
        <div className="hidden h-20 leading-[80px] transition cursor-pointer md:block">
          <div className="transition hover:scale-150 hover:translate-y-2 hover:-translate-x-1">
            <AvatarIcon></AvatarIcon>
          </div>
        </div>
      </Dropdown>
      <AuthCard />
    </div>
  )
}
export default Avatar
