import { Form, Input, message } from 'antd'
import { $api } from '@/http/api'
const RestaurantApplyForm = (props) => {
  const { close } = props
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 8 },
      md: { span: 4 }
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 16 },
      md: { span: 20 }
    }
  }
  const submit = (values) => {
    $api.user
      .applyRes({ ...values, longitude: 64.2, latitude: 38.3 })
      .then((res) => {
        if (res.code === 200) {
          message.success(res.message)
          close()
        }
      })
  }
  return (
    <Form
      initialValues={{}}
      {...formItemLayout}
      autoComplete="off"
      onFinish={submit}
      className="md:!px-10 "
    >
      <Form.Item
        label="Restaurant Name"
        name="name"
        rules={[{ required: true, message: 'Please input restaurant name' }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Description"
        name="description"
        rules={[{ required: true, message: 'Please input description' }]}
      >
        <Input placeholder="descirbe your restaurant" />
      </Form.Item>

      <Form.Item
        label="Restaurant Address"
        name="address"
        rules={[{ required: true, message: 'Please input address' }]}
      >
        <Input placeholder="input your restaurant location" />
      </Form.Item>

      <Form.Item
        label="Owner Name"
        name="owner_name"
        rules={[{ required: true, message: 'Please input owner name' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Service Type"
        name="service_type"
        rules={[{ required: true, message: 'Please input service type' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Phone Number"
        name="phone"
        rules={[{ required: true, message: 'Please input phone number' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item wrapperCol={{ span: 24 }}>
        <button
          type="submit"
          className="float-right px-6 py-3 text-xl text-white transition bg-red-400 cursor-pointer w-fit rounded-3xl hover:translate-x-3"
        >
          Submit
        </button>
      </Form.Item>
    </Form>
  )
}

export default RestaurantApplyForm
