import React, { useEffect, useState } from 'react'
import { message, Modal } from 'antd'
import { useAuth } from '@/utils/auth'
import { $api } from '@/http/api'
import RestaurantApplyForm from './RestaurantApplyForm'
import { ArrowLeftOutlined } from '@ant-design/icons'
import store from '@/store'

const AuthCard = () => {
  const [visible, setVisible] = useState(true)
  const { setNewSignUp, getUser } = useAuth()
  const [cardState, setCardState] = useState(0)
  // TODO tmpAuth judge
  const tmpAuth = getUser().auth || 0
  const onClose = (e) => {
    setVisible(false)
    setNewSignUp(false)
  }
  const applyAuth = (auth) => {
    console.log(auth)
    switch (auth) {
      case 1:
        // delivery staff
        $api.user.changeAuth({ identity: 1 }).then((res) => {
          if (res.code === 200) {
            message.success(res.message)
            onClose()
          }
        })

        return
      case 2:
        // resaurant manager
        setCardState(auth)
        return
      default:
        return
    }
  }

  // back arrow icon
  const backArrow = () => {
    return (
      <ArrowLeftOutlined
        className="mr-2"
        onClick={(e) => {
          setCardState(0)
        }}
      />
    )
  }

  const title = () => {
    return cardState === 0 ? (
      'Join Us'
    ) : cardState === 1 ? (
      'Apply for Delivery Staff'
    ) : (
      <>
        {backArrow()}
        Apply for a Restaurant
      </>
    )
  }
  const applyRestaurant = () => {}
  return (
    <Modal
      title={<div className="text-2xl font-bold">{title()}</div>}
      centered
      visible={visible}
      className="!w-full md:!w-2/3"
      footer={null}
      onCancel={onClose}
    >
      {cardState === 0 ? (
        <div className="flex flex-col bg-right bg-no-repeat md:px-5 bg-auth-card">
          <div className="flex items-center h-24 font-bold md:gap-5">
            <div className="w-64">
              <div
                className="px-3 py-2 text-white transition bg-red-400 cursor-pointer md:px-6 md:py-3 text-md md:text-xl rounded-3xl hover:translate-x-3 w-fit"
                onClick={() => applyAuth(2)}
              >
                Restaurant Manager
              </div>
            </div>
            <div className="text-sm text-right md:text-gray-600 md:text-lg">
              You can become an restaurant manager
            </div>
          </div>
          <div className="flex items-center h-24 font-bold md:gap-5">
            <div className="w-64">
              <div
                className="px-5 py-2 text-white transition bg-blue-500 cursor-pointer md:px-6 md:py-3 md:text-xl text-md rounded-3xl hover:translate-x-3 w-fit"
                onClick={() => applyAuth(1)}
              >
                Delivery Staff
              </div>
            </div>
            <div className="text-sm text-right md:text-gray-600 md:text-lg">
              You can become an delivery staff
            </div>
          </div>
          <div className="flex items-center h-24 gap-5 font-bold">
            <div
              className="px-5 py-2 text-white transition bg-green-500 cursor-pointer md:px-6 md:py-3 text-md md:text-xl rounded-3xl hover:translate-x-3 w-fit"
              onClick={onClose}
            >
              Not Now, Enjoy TAKAWAY
            </div>
          </div>
        </div>
      ) : (
        // ) : cardState === 1 ? (
        //   '1'
        <RestaurantApplyForm close={onClose}></RestaurantApplyForm>
      )}
    </Modal>
  )
}
const authCard = () => {
  return (
    <>
      {/* render auth card */}
      {store.getState().global.newSignUp ? <AuthCard /> : ''}
    </>
  )
}
export default authCard
