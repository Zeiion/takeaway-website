import { Drawer, Button, Space, Radio, Menu } from 'antd'
import { useState } from 'react'
import { MenuOutlined } from '@ant-design/icons'
import AvatarIcon from '@/components/AvatarIcon/AvatarIcon'
import { useNavigate } from 'react-router-dom'
import { useAuth } from '@/utils/auth'
import AuthCard from '@/components/AuthCard/AuthCard'
const SideMenu = () => {
  const [visible, setVisible] = useState(false)
  const navigate = useNavigate()
  const { signOut, setNewSignUp, getUser } = useAuth()
  const onOpen = () => {
    setVisible(true)
  }
  const onClose = () => {
    setVisible(false)
  }

  const handleMenuClick = (e) => {
    switch (e.key) {
      case '0':
        navigate('/message')
        break
      case '1':
        setNewSignUp(true)
        break
      case '2':
        navigate('/login')
        signOut()
        break
      case '3':
        navigate('/orders')
        break
      case '4':
        let id = getUser().meta.restaurantId
        navigate(`/manager/${id}/orders`)
        break
      case '5':
        navigate('/rider')
        break
      case '6':
        navigate('/admin/apply')
        break
      default:
        break
    }
    setVisible(false)
  }

  let items = [
    { label: 'Message', key: '0' },
    { label: 'Join as Manager/Delivery Staff', key: '1' },
    { label: 'My Orders', key: '3' }
  ]

  let auth = getUser().auth

  if (auth === 1) {
    items = items.concat({ label: 'Delivery', key: '5' })
  } else if (auth === 2) {
    items = items.concat({ label: 'My Restaurant', key: '4' })
  } else if (auth === 3) {
    items = items.concat({ label: 'Manage Apply', key: '6' })
  }

  const menu = (
    <Menu
      onClick={handleMenuClick}
      items={[...items, { label: 'Log Out', key: '2' }]}
    />
  )
  return (
    <>
      <MenuOutlined onClick={onOpen} />
      <Drawer
        placement="left"
        width="80vw"
        onClose={onClose}
        visible={visible}
        title={<AvatarIcon></AvatarIcon>}
        extra={
          <div
            onClick={() => {
              navigate('/')
            }}
          >
            TAKAWAY
          </div>
        }
        closable={false}
      >
        {menu}
      </Drawer>
      <AuthCard />
    </>
  )
}

export default SideMenu
