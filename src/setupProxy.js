/**
 * To deal with cross domain problems
 */
const { createProxyMiddleware } = require('http-proxy-middleware')

module.exports = function (app) {
  app.use(
    createProxyMiddleware('/api', {
      // target: 'http://localhost:8080/', // backend address
      target:
        process.env.NODE_ENV === 'development'
          ? 'http://123.57.65.155:8080/'
          : 'http://123.57.65.155:8080/',
      // : 'http://127.0.0.1:4523/mock/930792/', // backend address
      // pathRewrite: {
      //   '^/api': '' // replace '/api' with ''
      // },
      changeOrigin: true
    })
  )
}
