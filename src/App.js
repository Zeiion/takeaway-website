import './App.less'
import { useRoutes } from 'react-router-dom'
import routes from '@/routes/config'
import { RouterAuth } from '@/routes/index'
// import { useViewport } from './utils/view'

const App = () => {
  const Element = useRoutes(routes)
  // const { width } = useViewport()
  // return Element
  return <RouterAuth>{Element}</RouterAuth>
}

export default App
