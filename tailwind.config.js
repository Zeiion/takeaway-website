module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      backgroundImage: {
        'auth-card': "url('@/assets/svg/apply.svg')",
        'location-card': "url('@/assets/svg/location.svg')"
      }
    }
  },
  plugins: []
}
